# umwelt.info development infrastructure automation

This project contains scripts and CI jobs to automate the setup and maintenance of the infrastructure supporting the umwelt.info development.

## Guiding principles

Our approach aims for efficiency, simplicity and automation.

We aim for _efficiency_ and try to apply green computing techniques to minimize the ecological footprint of our service.

To us, this also implies that we provide our service using as little infrastructure as possible. And what we do need, we keep as _simple_ as possible so that we are able to understand it, continuously optimize it and keep it secure.

Since our technical staff is limited, we also aim to _automate_ as much as we reasonable can so that our time can be spent on improving our service instead of maintaining our infrastructure.

## Technical approach

The development infrastructure is provisioned on demand and ideally consumes cloud resources only while a human is actively using it.

## Structural overview

```mermaid
graph

subgraph "internet"
    dev-a(developer a)
    dev-b(developer b)
end

subgraph "entwicklung"
    dev-vm-a[<b>entwicklung-a</b><br/>interactive usage]
    dev-vm-b[<b>entwicklung-b</b><br/>interactive usage]

    dev-a --> dev-vm-a 
    dev-b --> dev-vm-b 
end
```
