cd data-stories
git fetch
git reset --hard origin/main

cd /mnt/statistik
Rscript /home/ubuntu/data-stories/usage_stats/update_usage_stats_r
python3 /home/ubuntu/data-stories/usage_stats/update_publication_count.py
