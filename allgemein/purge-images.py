import sys

import openstack


if len(sys.argv) != 2:
    sys.exit("usage: purge-images.py <image-name>")

image_name = sys.argv[1]
old_image_name = f"{image_name}-veraltet"


connection = openstack.connect()

for image in connection.image.images(name=old_image_name):
    try:
        connection.image.delete_image(image)
    except Exception as exception:
        print(exception)
