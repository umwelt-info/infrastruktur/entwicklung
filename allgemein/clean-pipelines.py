import gitlab
import os

from datetime import datetime, timedelta


gl = gitlab.Gitlab(
    url="https://gitlab.opencode.de", private_token=os.environ["GL_PRIVATE_TOKEN"]
)

for project_id in (433, 574, 1486, 3856, 628, 2532, 2533, 2781):
    project = gl.projects.get(project_id)

    deadline = datetime.now() - timedelta(days=30)
    print(f"Cleaning pipelines of project {project.name}")

    for pipeline in project.pipelines.list(all=True):
        updated_at = datetime.strptime(pipeline.updated_at, "%Y-%m-%dT%H:%M:%S.%fZ")

        if updated_at < deadline:
            print(f"Deleteing pipeline {pipeline.id}, last updated on {updated_at}")
            pipeline.delete()
