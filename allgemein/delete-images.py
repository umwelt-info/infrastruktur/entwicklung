import sys

import openstack


if len(sys.argv) != 2:
    sys.exit("usage: delete-images.py <image-name>")

image_name = sys.argv[1]
old_image_name = f"{image_name}-veraltet"


connection = openstack.connect()

for image in connection.image.images(name=image_name):
    image.name = old_image_name
    connection.image.update_image(image)
