import sys
import openstack

from datetime import datetime, timedelta


if len(sys.argv) != 2:
    sys.exit("usage: backup.py <volume-name>")

volume_name = sys.argv[1]


connection = openstack.connect()

volume = connection.block_storage.find_volume(volume_name)

now = datetime.now()
keep_daily = timedelta(days=5)
keep_weekly = timedelta(days=30)
keep_monthly = timedelta(days=90)
to_delete = []

for backup in connection.block_storage.backups(volume_id=volume.id):
    created_at = datetime.strptime(backup.created_at, "%Y-%m-%dT%H:%M:%S.%f")

    age = now - created_at + timedelta(hours=12)

    if age <= keep_daily:
        continue

    if age <= keep_weekly and created_at.weekday() == 6:
        continue

    if age <= keep_monthly and created_at.day == 1:
        continue

    to_delete.append(backup)

for backup in to_delete:
    print(
        f"Deleting backup {backup.name} ({backup.id}), created at {backup.created_at}"
    )
    connection.block_storage.delete_backup(backup)

backup = connection.block_storage.create_backup(
    name=f"{volume.name}-{now:%Y-%m-%d}",
    description=f"scheduled backup, created at {now:%Y-%m-%dT%H:%M:%S}",
    volume_id=volume.id,
    force=True,
)
print(
    f"Created backup {backup.name} ({backup.id}) of volume {volume.name} ({volume.id})"
)
