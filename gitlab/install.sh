export DEBIAN_FRONTEND=noninteractive

apt-get update --yes
apt-get full-upgrade --yes

apt-get install --yes \
    fail2ban \
    python3-openstackclient \
    unzip

echo "Cache=no" >>/etc/systemd/resolved.conf

systemctl restart systemd-resolved.service

cat >>/etc/systemd/journald.conf <<EOF
MaxRetentionSec=90day
MaxFileSec=1week
EOF

systemctl restart systemd-journald.service

useradd --create-home --shell /usr/sbin/nologin jump

cat >/etc/ssh/sshd_config.d/jump.conf <<EOF
Match User jump
    PermitTTY no
    PermitListen none
    PermitOpen *:22
EOF

curl -1sLf "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash

apt-get install --yes gitlab-runner

mkdir /home/gitlab-runner/.ssh

ssh-keygen -t ed25519 -N '' -f /home/gitlab-runner/.ssh/id_ed25519

cat >/home/gitlab-runner/.ssh/config <<EOF
StrictHostKeyChecking accept-new
EOF

chown -R gitlab-runner:gitlab-runner /home/gitlab-runner/.ssh

cat >/home/gitlab-runner/.profile <<EOF
export OS_CLOUD=entwicklung
EOF

chown gitlab-runner:gitlab-runner /home/gitlab-runner/.profile

fallocate --length 1G /swapfile
chmod 600 /swapfile
mkswap /swapfile
echo '/swapfile none swap sw 0 0' >>/etc/fstab
swapon --all
