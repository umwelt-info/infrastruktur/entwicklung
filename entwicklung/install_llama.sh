#!/bin/bash
set -ex

URL=https://huggingface.co/Mozilla/Llama-3.2-3B-Instruct-llamafile/resolve/main/Llama-3.2-3B-Instruct.Q6_K.llamafile
LLAMAFILE=$(basename $URL)

INSTALL_DIR=$HOME/llamafile
BIN_DIR=$HOME/.local/bin

mkdir -p $INSTALL_DIR
curl --proto '=https' --tlsv1.2 -sSL -o $INSTALL_DIR/$LLAMAFILE $URL
chmod +x $INSTALL_DIR/$LLAMAFILE

mkdir -p $BIN_DIR
ln -s $INSTALL_DIR/$LLAMAFILE $BIN_DIR/llama

echo 'alias llama-start-server="llama --server --nobrowser"' >> $HOME/.bash_aliases
