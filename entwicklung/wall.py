#!/usr/bin/env python3

import subprocess
import os
from pathlib import Path

import xdg


bin_path = (
    next(Path("~/.vscodium-server/bin").expanduser().iterdir())
    / "bin/remote-cli/codium"
)

msg_path = Path("/tmp/automatic_shutdown_notice")

msg_path.write_text("""This system will be automatically shut down in 5 minutes.

Please create /home/ubuntu/please_dont_die if you want to continue to use it.""")

for sock_path in xdg.XDG_RUNTIME_DIR.iterdir():
    if not sock_path.name.startswith("vscode-ipc-") or not sock_path.name.endswith(
        ".sock"
    ):
        continue

    env = os.environ
    env["VSCODE_IPC_HOOK_CLI"] = str(sock_path)

    process = subprocess.run([bin_path, "--reuse-window", msg_path], env=env)
    if process.returncode == 0:
        break
