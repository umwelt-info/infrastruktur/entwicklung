declare -r -A KNOWN_LOGINS=(
    [OC000008353898]="adam"
    [OC000008373193]="jakob"
    [OC000008786379]="stefan"
    [OC000020314828]="vogeljoh"
    [OC000019649294]="karsten"
    [OC000021124312]="berthold"
    [OC000021106231]="hessefa"
    [OC000021809054]="mohrti"
    [OC000022531492]="rauh"
    [OC000032393429]="oc000032393429"
    [OC000031025361]="albrechtl"
    [MichelFrerk]="frerk"
    [OC000037364913]="haedke"
    [oc000056025066]="bergerb"
    [oc000057191195]="weyers"
)

if [ -n "$NUTZER" ]; then
    export NUTZER="${NUTZER,,}"
elif [ -n "$GITLAB_USER_LOGIN" ]; then
    export NUTZER="${KNOWN_LOGINS[$GITLAB_USER_LOGIN]}"
fi

echo "Established user as ${NUTZER}"
