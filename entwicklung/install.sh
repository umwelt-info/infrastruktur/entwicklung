export DEBIAN_FRONTEND=noninteractive

sudo --preserve-env=DEBIAN_FRONTEND apt-get update --yes
sudo --preserve-env=DEBIAN_FRONTEND apt-get full-upgrade --yes

sudo --preserve-env=DEBIAN_FRONTEND apt-get install --no-install-recommends --yes \
	build-essential git-lfs cmake mold clang \
	pkg-config libssl-dev libsqlite3-dev libpoppler-glib-dev libqpdf-dev \
	ripgrep fd-find hyperfine parallel zip sqlite3 \
	xattr unzip libxml2-utils poppler-utils \
	python3-pip python3-ipykernel \
	python3-geojson python3-folium python3-tomli-w python3-cssselect \
	python3-pandas python3-seaborn \
	python3-nltk python3-sklearn python3-wordcloud \
	python3-gitlab python3-xdg \
	pipx \
	php-opcache php-apcu php-pdo php-sqlite3 \
	php-dom php-xml php-zip php-gd \
	composer \
	r-cran-tidyverse r-cran-irkernel r-cran-jsonlite \
	r-cran-tm r-cran-wordcloud r-cran-snowballc \
	r-cran-lubridate r-cran-httr2 \
	docker-compose-v2 \
	npm node-node-sass

sudo snap install chromium

sudo tee /etc/security/limits.d/nofile.conf <<EOF
ubuntu soft nofile 1048576
EOF

curl --proto '=https' --tlsv1.2 -sSfL "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | \
	sudo env os=ubuntu dist=noble bash

sudo --preserve-env=DEBIAN_FRONTEND apt-get install --yes gitlab-runner

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | \
	sh -s -- -y --profile minimal --component rustfmt clippy

pipx install ruff pylyzer quarto-cli

Rscript -e "dir.create(path = Sys.getenv('R_LIBS_USER'), showWarnings = FALSE, recursive = TRUE)"
Rscript -e "install.packages(c('leaflet', 'leaflet.extras', 'geojsonsf'))"

sudo tee /etc/php/8.3/cli/conf.d/raise-upload-limit.ini <<EOF
upload_max_filesize = 50M
post_max_size = 50M
EOF
sudo chmod +x /etc/php/8.3/cli/conf.d/raise-upload-limit.ini

echo 'export PATH=$HOME/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/bin:$PATH' >> .profile
echo 'export PYTHONPATH=$HOME/data-stories:$PYTHONPATH' >> .profile
source .profile

echo 'alias harvester="clear; cargo fmt && cargo xtask harvester"' >> .bash_aliases
echo 'alias fix-ssh-auth="export SSH_AUTH_SOCK=\$(ls -t /tmp/ssh-*/agent.* | head -1)"' >> .bash_aliases
echo 'alias py-venv-activate="source $HOME/data-stories/.venv/bin/activate"' >> .bash_aliases
echo 'alias gitlabrunner-stop="sudo systemctl stop gitlab-runner && sudo docker system prune --all --force"' >> .bash_aliases
echo 'alias create-harvester-toml="$HOME/data-stories/metadata/create_filtered_harvester.toml.py"' >> .bash_aliases

mkdir --parents .local/share/bash-completion/completions
rustup completions bash > .local/share/bash-completion/completions/rustup
rustup completions bash cargo > .local/share/bash-completion/completions/cargo

CARGO_PROFILE_RELEASE_LTO=thin cargo install --locked typst-cli typstyle

sudo --preserve-env=DEBIAN_FRONTEND apt-get clean

for projekt in metadaten search-ui content-ui data-stories journal-web-ui usage-stats-api infrastruktur/entwicklung infrastruktur/testbetrieb infrastruktur/wirkbetrieb infrastruktur/lasttest infrastruktur/datacube
do
    git clone https://gitlab.opencode.de/umwelt-info/$projekt.git
done

pushd metadaten
cargo xtask
cargo xtask test
cargo build --package=harvester
cargo build --bin=indexer
cargo build --package=server
popd

pushd search-ui
npm install
popd

pushd content-ui
composer install

pushd web/themes/custom/umweltinfo
npm install
popd

popd

pushd data-stories

python3 -m venv --system-site-packages .venv

bash <<EOF
source .venv/bin/activate
pip install spacy polars "numpy<2"
python -m spacy download de
python -m spacy download en
python -m nltk.downloader stopwords punkt
EOF

popd
