echo "Stashing VM of ${NUTZER}..."

openstack server stop entwicklung-$NUTZER
until openstack server show entwicklung-$NUTZER --column status | grep -q SHUTOFF; do sleep 1s; done

python3 allgemein/delete-images.py entwicklung-$NUTZER
openstack server image create entwicklung-$NUTZER --wait

openstack server delete entwicklung-$NUTZER --wait
python3 allgemein/purge-images.py entwicklung-$NUTZER
