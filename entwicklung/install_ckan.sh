#!/bin/bash
set -ex

git clone https://github.com/ckan/ckan-docker
pushd ckan-docker

export INSTALL_DIR=$PWD

cp .env.example .env

sed -i .env -e 's,envvars",envvars harvest ckan_harvester",' .env

sed -i ckan/Dockerfile -e "s,# See Dockerfile.dev for more details and examples,USER root\nRUN pip3 install -e 'git+https://github.com/ckan/ckanext-harvest.git@master#egg=ckanext-harvest' \&\& pip3 install -r \${APP_DIR}/src/ckanext-harvest/pip-requirements.txt,"

popd

echo "alias ckan-up=\"bash -c 'cd $INSTALL_DIR && sudo docker compose up --build'\"" >> $HOME/.bash_aliases
echo "alias ckan-gather=\"bash -c 'cd $INSTALL_DIR && sudo docker compose exec ckan ckan harvester gather-consumer'\"" >> $HOME/.bash_aliases
echo "alias ckan-fetch=\"bash -c 'cd $INSTALL_DIR && sudo docker compose exec ckan ckan harvester fetch-consumer'\"" >> $HOME/.bash_aliases
echo "alias ckan-run=\"bash -c 'cd $INSTALL_DIR && sudo docker compose exec ckan ckan harvester run'\"" >> $HOME/.bash_aliases
