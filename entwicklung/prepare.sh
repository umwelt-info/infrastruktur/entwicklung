for projekt in metadaten search-ui content-ui data-stories journal-web-ui usage-stats-api infrastruktur/entwicklung infrastruktur/testbetrieb infrastruktur/wirkbetrieb infrastruktur/lasttest infrastruktur/datacube
do
    pushd $(basename $projekt)
    git fetch
    git reset --hard origin/main
    git remote set-url origin git@gitlab.opencode.de:umwelt-info/$projekt.git
    popd
done

sudo gitlab-runner register --non-interactive \
    --url="https://gitlab.opencode.de" --token="$GITLAB_RUNNER_TOKEN" \
    --name="$(hostname)" --executor="docker" --docker-image="ubuntu:latest"

sudo systemctl restart gitlab-runner.service

export DEBIAN_FRONTEND=noninteractive

sudo --preserve-env=DEBIAN_FRONTEND apt-get update --yes
sudo --preserve-env=DEBIAN_FRONTEND apt-get full-upgrade --yes
